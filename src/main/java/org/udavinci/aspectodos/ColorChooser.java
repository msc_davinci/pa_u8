package org.udavinci.aspectodos;

import java.awt.*;
import java.util.HashMap;
import java.util.Set;

public class ColorChooser {
    private HashMap<String, Color> hashMap;

    public ColorChooser() {
        hashMap = new HashMap<String, Color>();

        hashMap.put("black", Color.BLACK);
        hashMap.put("blue", Color.BLUE);
        hashMap.put("cyan", Color.CYAN);
        hashMap.put("darkGray", Color.DARK_GRAY);
        hashMap.put("gray", Color.GRAY);
        hashMap.put("green", Color.GREEN);
        hashMap.put("lightGray", Color.LIGHT_GRAY);
        hashMap.put("magenta", Color.MAGENTA);
        hashMap.put("orange", Color.ORANGE);
        hashMap.put("pink", Color.PINK);
        hashMap.put("red", Color.RED);
        hashMap.put("white", Color.WHITE);
        hashMap.put("yellow", Color.YELLOW);
    }

    public Color getColor(String name) {
        return hashMap.get(name);
    }

    public Set<String> getKeySet() {
        return hashMap.keySet();
    }
}
