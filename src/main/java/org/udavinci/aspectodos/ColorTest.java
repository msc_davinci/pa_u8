package org.udavinci.aspectodos;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

public class ColorTest extends JFrame {
    private JList colorList;
    private ColorChooser chooser;
    private JPanel drawPanel;

    public ColorTest() {
        super("List Test");

        setLayout(new GridLayout(2, 1));

        chooser = new ColorChooser();

        colorList = new JList(chooser.getKeySet().toArray());
        colorList.setVisibleRowCount(5);
        colorList.setSelectedIndex(0);

        colorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JPanel choosePanel = new JPanel();

        choosePanel.add(new JScrollPane(colorList));

        colorList.addListSelectionListener(

                new ListSelectionListener() {

                    public void valueChanged(ListSelectionEvent event) {
                        repaint();
                    }
                }
        );

        drawPanel = new JPanel(); // create a drawing panel
        drawPanel.setBackground(Color.WHITE);

        add(drawPanel);
        add(choosePanel);

        setSize(300, 300);
        setVisible(true);
    }

    public void paint(Graphics g) {
        super.paint(g);

        Object selected = colorList.getSelectedValue();

        g.setColor(chooser.getColor((String) selected));

        Rectangle bounds = drawPanel.getVisibleRect();
        g.fillRect(bounds.x + 10, bounds.y + 30, bounds.width - 15,
                bounds.height - 15);
    }

    public static void main(String args[]) {
        ColorTest application = new ColorTest();
        application.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
